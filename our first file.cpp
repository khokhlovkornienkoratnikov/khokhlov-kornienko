#include <iostream>
using namespace std;

void FillMassive(int Massive[19], int count, int diapason, int lowerLimit)
{
	srand(time(NULL));
	using namespace std;
	system("cls");
	for (int i = 1; i <= count; i++)
	{
		Massive[i] = lowerLimit + rand() % diapason;
	}
}

void SortDown(int Massive[19], int count)
{
	using namespace std;
	int maxValue, maxIndex, buffer;

	system("cls");
	for (int i = 1; i < count; i++)
	{
		maxValue = Massive[i];
		maxIndex = i;
		for (int j = (i + 1); j <= count; j++)
		{
			if (Massive[j] > maxValue)
			{
				maxValue = Massive[j];
				maxIndex = j;
			}
			if (j == count)
			{
				buffer = Massive[i];
				Massive[i] = maxValue;
				Massive[maxIndex] = buffer;
			}
		}
	}
	cout << "DONE.";
	system("pause");
}

void OutputRepeatable(int Massive[19], int count)
{
	int Massive2[LengthOfMassive + 1];
	int unrepeatable[LengthOfMassive + 1];

	for (int i = 1; i <= 18; i++)
	{
		Massive2[i] = Massive[i];
	}
	using namespace std;
	system("cls");

	int repeatable = 0;
	int index = 0;

	for (int i = 1; i <= count; i++)
	{
		for (int j = 1; j <= count; j++)
		{
			if (Massive2[i] == Massive2[j])
			{
				repeatable++;
			}
		}
		if (repeatable == 1)
		{
			index++;
			unrepeatable[index] = Massive2[i];
		}
		repeatable = 0;
	}

	int maxValue, maxIndex, buffer;

	for (int i = 1; i < (count); i++)
	{
		maxValue = Massive2[i];
		maxIndex = i;
		for (int j = (i + 1); j <= (count); j++)
		{
			if (Massive2[j] > maxValue)
			{
				maxValue = Massive2[j];
				maxIndex = j;
			}
			if (j == (count))
			{
				buffer = Massive2[i];
				Massive2[i] = maxValue;
				Massive2[maxIndex] = buffer;
			}
		}
	}

	for (int i = 1; i <= count; i++)
	{
		if ((Massive2[i - 1] == Massive2[i]) || (Massive2[i + 1] == Massive2[i]))
		{
			repeatable++;
			cout << Massive2[i] << " ";
		}
	}

	if (repeatable == 0)
	{
		cout << "There are no repeatable elements" << endl;
	}
	for (int i = 1; i <= index; i++)
	{
		cout << unrepeatable[i] << " ";
	}
	cout << "\nDONE." << endl;
	system("pause");
}
void SetRange(int& diapason,int &lowerLimit)
{
	using namespace std;

	int upperLimit;
	system("cls");
	do
	{
		cout << "Enter the lower limit of the array " << endl;
		cin >> lowerLimit;
		cout << "Enter the upper limit of the array " << endl;
		cin >> upperLimit;
		if (lowerLimit > upperLimit)
		{
			cout << "The data you entered is not logical. Enter again" << endl;
		}
	}
	while (upperLimit <= lowerLimit);
	diapason = upperLimit - lowerLimit + 1;
}

void Change(int(numbers[19]), int count)
{
	using namespace std;
	int i, replacement;
	system("cls");
	cout << "Enter the number you would like to see instead of zeros: " << endl;
	cin >> replacement;
	for (i = 1; i <= count; i++)
	{
		if (numbers[i] == 0)
		{
			numbers[i] = replacement;
		}
	}
	cout << "DONE." << endl;
	system("pause");
}

void Summation(int numbers[19], int count, int &sumResult)
{
	using namespace std;
	char choice;
	int i;
	system("cls");
	sumResult = 0;
	cout << "Press \"P\" to sum positive or press \"N\" to sum negative numbers of array" << endl;
	do
	{
		cin >> choice;
		if ((choice != 'p') || (choice != 'n') || (choice != 'P') || (choice != 'N'))
		{
			break;
		}
	}
	while (true);
	if ((choice == 'p') || (choice == 'P'))
	{
		for (i = 1; i <= count; i++)
		{
			if (numbers[i] > 0)
			{
				sumResult = sumResult + numbers[i];
			}
		}
		cout << sumResult << endl;
	}
		if ((choice == 'n') || (choice == 'N'))
		{
			for (i = 1; i <= count; i++)
			{
				if (numbers[i] < 0)
				{
					sumResult = sumResult + numbers[i];
				}
			}
			cout << sumResult << endl;
		}
	//cout << sumResult << endl;
	system("pause");
}

int main()
{
	using namespace std;
	setlocale(0, "");
	srand(time(NULL));

	char option;
	int diapason = 34;
	int lowerLimit = (-12);
	int sumResult = 0;

	FillMassive(Massive, LengthOfMassive, diapason, lowerLimit);

	do
	{
		cout << "1. Output massive on display";
		cout << "\n2. Set range of massive";
		cout << "\n3. Sort descending";
		cout << "\n4. Replace \"0\" elements with vale equal to selected number";
		cout << "\n5. Summ positive or negative elements of array at user's choice";
		cout << "\n6. Repeatable elements";
		cout << "\n7. Random";
		cout << "\nPress \"e\" to exit\n";
		option = _getch();
		switch (option)
		{
		case '1':OutputMassive(Massive, LengthOfMassive); break;
		case '2':SetRange(diapason, lowerLimit); break;
		case '3':SortDown(Massive, LengthOfMassive); break;
		case '4':Change(Massive, LengthOfMassive); break;
		case '5':Summation(Massive, LengthOfMassive, sumResult); break;
		case '6':OutputRepeatable(Massive, LengthOfMassive); break;
		case '7':
		{
			FillMassive(Massive, LengthOfMassive, diapason, lowerLimit);
			cout << "DONE." << endl;
			system("pause");
		}
		}
		system("cls");
		if ((option == 'e') or (option == 'E'))
		{
			break;
		}
	} while (true);
}


